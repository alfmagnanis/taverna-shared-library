import { Component, OnInit } from '@angular/core';
import { EventosService } from '../taverna-shared-library/services/eventos/eventos.services'; 
import { debug } from 'util';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  private events: any;

  constructor(private eventosService: EventosService) {}

  ngOnInit() {
    this.eventosService.get().then( 
      (value) =>{
        debugger;
        this.events = value;
      }
    )
  }

  title = 'app';
}

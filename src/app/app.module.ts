import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { TavernaSharedModule } from '../taverna-shared-library/taverna-shared.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    TavernaSharedModule.forRoot({ serverUrl: 'http://blog-tavernaonline-com-br.umbler.net'})
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

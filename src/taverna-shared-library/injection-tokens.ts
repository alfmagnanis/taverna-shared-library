import { InjectionToken } from '@angular/core';

import { Config } from './models/config';

export const TAVERNA_SHARED_CONFIG = new InjectionToken<Config>('TAVERNA_SHARED_CONFIG');
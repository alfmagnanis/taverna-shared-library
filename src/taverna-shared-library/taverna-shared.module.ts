import { NgModule, ModuleWithProviders } from '@angular/core';

import { HttpModule } from '@angular/http';

import { Config } from './models/config';
import { TAVERNA_SHARED_CONFIG } from './injection-tokens';
import { EventosService } from './services/eventos/eventos.services';

@NgModule({
    imports: [
      HttpModule
    ],
    providers: [EventosService],
  })
  export class TavernaSharedModule { 

    public static forRoot(config: Config): ModuleWithProviders {
        return {
            ngModule: TavernaSharedModule,
            providers: [
                { provide: TAVERNA_SHARED_CONFIG, useValue: config },
            ],
        };
    }
}
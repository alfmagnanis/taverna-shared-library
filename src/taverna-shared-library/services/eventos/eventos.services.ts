import { Injectable, Inject } from '@angular/core';
import { Http } from '@angular/http';
// import { Observable } from 'rxjs';
// import { map } from 'rxjs/operators/map';
// import Observable.of from 'rxjs/Observable/of';
// import 'rxjs/Observable/of';

import { TAVERNA_SHARED_CONFIG } from '../../injection-tokens'
import { Config } from '../../models/config';

@Injectable()
export class EventosService {
  private static data; 

  constructor(public http: Http, @Inject(TAVERNA_SHARED_CONFIG) private config: Config) {
  }

  async get(): Promise<any> {
    if (EventosService.data) {
      return Promise.resolve(EventosService.data);
    }
  
    return new Promise(resolve => {
      this.http.get(`${this.config.serverUrl}/api/eventos/eventos`)
      .subscribe((data: any) => {
          EventosService.data = data._body;
          resolve(EventosService.data);
        });
    });
  }
}
